package com.selenium.java.base.pages.android;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class AddAdScreen extends BaseTest {

//region FindBy

    @FindBy(how = How.ID, using = "pl.tablica:id/list_it")
    public WebElement btn_list_it;

    @FindBy(how = How.ID, using = "pl.tablica:id/inputText")
    public WebElement btn_tytul;

    @FindBy(how = How.ID, using = "pl.tablica:id/chooserBtn")
    public WebElement btn_chooserBtn;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[4]/android.widget.TextView\n")
    public WebElement btn_domIOgrod;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[9]\n")
    public WebElement btn_pozostale;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Prywatne')]")
    public WebElement btn_prywatne;

    @FindBy(how = How.ID, using = "pl.tablica:id/priceInput")
    public WebElement btn_priceInput;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Używane')]")
    public WebElement btn_uzywane;

    @FindBy(how = How.ID, using = "pl.tablica:id/inputText")
    public WebElement input_addDescription;

    @FindBy(how = How.ID, using = "pl.tablica:id/inputText")
    public WebElement btn_inputContact;

    @FindBy(how = How.ID, using = "pl.tablica:id/checkbox")
    public WebElement btn_checkbox;

    @FindBy(how = How.ID, using = "pl.tablica:id/previewAdBtn")
    public WebElement btn_preview;

    //endregion FindBy

    public void addAdScreen(String tytul, String cena, String trescOgloszenia, String uzytkownik) {

        MethodHelper helper = new MethodHelper();

        btn_list_it.click();
        btn_chooserBtn.click();
        btn_domIOgrod.click();
        btn_pozostale.click();
        btn_tytul.sendKeys(tytul);
        btn_prywatne.click();

        helper.swipeInDirection(MethodHelper.direction.UP, "up", 0.5);

        btn_priceInput.sendKeys(cena);

        input_addDescription.sendKeys(trescOgloszenia);

        helper.swipeInDirection(MethodHelper.direction.UP, "up", 0.8);
        helper.swipeInDirection(MethodHelper.direction.UP, "up", 0.8);

        btn_inputContact.sendKeys(uzytkownik);
        btn_checkbox.click();
        btn_preview.click();

    }
}











