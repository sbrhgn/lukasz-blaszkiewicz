package com.selenium.java.base.pages.android;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class CreateAccountScreen extends BaseTest {

    //region FindBy
    @FindBy(how = How.ID, using = "pl.tablica:id/createAccountBtn")
    public WebElement btn_createAccountBtn;

    @FindBy(how = How.ID, using = "pl.tablica:id/tabRegister")
    public WebElement btn_tabRegister;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'E-mail')]")
    public WebElement input_email;

    @FindBy(how = How.ID, using = "pl.tablica:id/input")
    public WebElement input_password;

    @FindBy(how = How.ID, using = "pl.tablica:id/checkbox")
    public WebElement btn_checkBox;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Załóż konto')]")
    public WebElement btn_register;
    //endregion FindBy

    public void createAccount(String name, String pass) {

        MethodHelper helper = new MethodHelper();

        btn_createAccountBtn.click();
        btn_tabRegister.click();
        input_email.sendKeys(name);
        input_password.sendKeys(pass);

        helper.waitTime(2);
        helper.swipeInDirection(MethodHelper.direction.UP, "up", 0.8);

        btn_checkBox.click();
        btn_register.click();

    }
}
















