package com.selenium.java.base.pages.web;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPage extends BaseTest {

    MethodHelper helper = new MethodHelper();


    @FindBy(how = How.ID, using = "")
    public WebElement btn_;

    @FindBy(how = How.ID, using = "my-account-link")
    public WebElement input_myAccont;

    @FindBy(how = How.ID, using = "userEmail")
    public WebElement input_email;

    @FindBy(how = How.ID, using = "userPass")
    public WebElement input_password;

    @FindBy(how = How.CLASS_NAME, using = "cookiesBarClose")
    public WebElement btn_closeCookies;

    @FindBy(how = How.ID, using = "se_userLogin")
    public WebElement btn_login;


    public void loginToApp(String name, String pass) {

        input_myAccont.click();
        input_email.sendKeys(name);
        input_password.sendKeys(pass);
        btn_closeCookies.click();
        btn_login.click();

    }

}
