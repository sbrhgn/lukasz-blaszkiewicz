package com.selenium.java.tests.presentation.presentation;

import com.selenium.java.tests.presentation.presentation.Car;

public class Car2 extends Car {

    public static void main(String[] args) {

        startEngine();

        myCar(Colors.RED);

        System.out.println("Force driver to fasten seat belts.");

        stopEngine();
    }
}
