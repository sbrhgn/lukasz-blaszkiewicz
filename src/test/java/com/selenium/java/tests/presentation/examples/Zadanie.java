package com.selenium.java.tests.presentation.examples;

public class Zadanie {

    public static void main(String[] args) {

        Projekt1 Marek = new Projekt1();
        Projekt1 Dawid = new Projekt1();
        Projekt1 Lukasz = new Projekt1();

        System.out.format("Srednia ocen dla ucznia Marek wynosi: %.2f%n", Marek.obliczSrednia(4, 3.5, 5, 2.5, 3.5, 4, 5, 6, 3));
        System.out.format("Srednia ocen dla ucznia Dawid wynosi: %.2f%n", Dawid.obliczSrednia(2, 2.5, 5, 6, 1, 3.5, 4.5, 2));
        System.out.format("Srednia ocena dla ucznia Lukasz wynosi: %.2f%n", Lukasz.obliczSrednia(6, 6, 6, 4, 3.5, 3, 1, 1, 1.5, 5, 6));

    }
}