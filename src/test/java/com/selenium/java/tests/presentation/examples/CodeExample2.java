package com.selenium.java.tests.presentation.examples;

public class CodeExample2 {
    public static void main(String[] args) {

        Person lukasz = new Person();
        lukasz.name = "Łukasz";
        lukasz.age = 26;
        lukasz.isAlive = true;
        Person michal = new Person();
        michal.name = "Michał";
        michal.age = 200;
        michal.isAlive = false;
        lukasz.przedstawSie();
        michal.przedstawSie();

//
        Person marek = new Person();
        marek.przedstawSie("Marek", 18, true);
        Person andrzej = new Person();
        andrzej.przedstawSie("Andrzej", 88, false);
    }
}
