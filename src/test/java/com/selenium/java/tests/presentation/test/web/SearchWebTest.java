package com.selenium.java.tests.presentation.test.web;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import com.selenium.java.base.pages.web.LoginPage;
import com.selenium.java.base.pages.web.SearchPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

import javax.jws.Oneway;

public class SearchWebTest extends BaseTest {

    MethodHelper helper= new MethodHelper();

    @BeforeMethod
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = true; //uncomment if u want a headless mode
        url = "https://www.olx.pl";

        launchWeb(platform, browser, context);
        System.out.println("Test started");
    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
                {"celestiallucifer@gmail.com", "Xxxxxx11pkdw", "Komoda"},
                {"celestiallucifer@gmail.com", "Xxxxxx11pkdw", "Rower górski"},
                {"celestiallucifer@gmail.com", "Xxxxxx11pkdw", "Szafa"},
        };
    }



    @Test(dataProvider = "getData")
    public void searchPage(String name, String pass, String title) {

        WebDriver driver = getDriver();

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        SearchPage searchPage = PageFactory.initElements(driver, SearchPage.class);
        loginPage.loginToApp(name, pass);
        searchPage.search(title);
        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"contact_methods\"]/li[1]/a")).isDisplayed());
        helper.getScreenShot("WEB: Search test.png");
    }

    @AfterMethod
    public void tearDown() {

        WebDriver driver = getDriver();
        System.out.println("Test ended");
        driver.close();
    }
}
