package com.selenium.java.tests.presentation.presentation;

public class Switch {

    private enum Colors {
        BLUE,
        RED,
        YELLOW,
        PINK,
        NONE
    }

    public static void main(String[] args) {
        ChooseColor(Colors.RED);
        ChooseColor(Colors.BLUE);
        ChooseColor(Colors.NONE);
        ChooseColor(Colors.YELLOW);
        ChooseColor(Colors.PINK);
    }

    public static void ChooseColor(Colors colors) {
        switch (colors) {

            case RED:
                System.out.println("yay! It's red!");
                break;

            case BLUE:
                System.out.println("yay! It's blue!");
                break;

            case YELLOW:
                System.out.println("yay! It's yelow!");
                break;

            case PINK:
                System.out.println("yay! It's pink!");
                break;

            default:
                System.out.println("none...");

        }
    }
}