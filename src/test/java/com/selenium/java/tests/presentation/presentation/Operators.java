package com.selenium.java.tests.presentation.presentation;

public class Operators {

    public static void main(String[] args) {

        int a = 17;
        int b = 4;
        int c = a + b;

        System.out.println(c); // = 21

        c = a + b;

        System.out.println(c); // = 13

        c = a * b;

        System.out.println(c); //= 68

        c = a / b;

        System.out.println(c); // = 4

        c = a % b;

        System.out.println(c); // = 1
    }
}