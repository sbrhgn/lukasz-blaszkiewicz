package com.selenium.java.tests.presentation.obiektowe;

import java.sql.SQLOutput;

public class ProgramowanieObiektowe {

    public static void main(String[] args) {

        class Monitor {
            int wysokosc;
            int szerokosc;

        }


        Monitor lg = new Monitor();
        lg.szerokosc = 124;
        lg.wysokosc = 200;

        Monitor asus = new Monitor();
        asus.szerokosc = 300;
        asus.wysokosc = 125;

        System.out.println(lg.szerokosc);
        System.out.println(lg.wysokosc);
        System.out.println(asus.szerokosc);
        System.out.println(asus.wysokosc);
    }
    }

