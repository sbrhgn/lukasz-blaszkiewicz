package com.selenium.java.tests.presentation.test.web;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import com.selenium.java.base.pages.android.CreateAccountScreen;
import com.selenium.java.base.pages.android.LoginScreen;
import com.selenium.java.base.pages.web.AddAdPage;
import com.selenium.java.base.pages.web.LoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.awt.*;

public class AddAdWebTest extends BaseTest {

    MethodHelper helper = new MethodHelper();

    @BeforeMethod
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false; //uncomment if u want a headless mode
        url = "https://www.olx.pl";

        launchWeb(platform, browser, context);
        System.out.println("Test started");
    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
                {"celestiallucifer@gmail.com", "Xxxxxx11pkdw", "Świerk", "50", "Roślina ogrodowa, szybko rosnąca tudzież w ogóle", "Nasturcja Lover", "C:\\Users\\Programista\\Desktop\\swierk.png"},
                {"celestiallucifer@gmail.com", "Xxxxxx11pkdw", "Basen", "150", "Basen ogrodowy, nieprzeciekąjacy", "Marek Makowski","C:\\Users\\Programista\\Desktop\\basen.png"},
                {"celestiallucifer@gmail.com", "Xxxxxx11pkdw", "Paprotka", "5", "Rosnaca nieprzerwanie od czasu PRL", "UB General","C:\\Users\\Programista\\Desktop\\paprotka.png"},
                {"celestiallucifer@gmail.com", "Xxxxxx11pkdw", "Róże ogrodowe", "8", "Likwidacja kwiaciarni - tylko priv", "Mariola Kwiatkowska","C:\\Users\\Programista\\Desktop\\roze.png"},
                {"celestiallucifer@gmail.com", "Xxxxxx11pkdw", "Kaktus", "20", "Latwy w utrzymaniu kwiat; polecany gdy studnia znajduje się 3 kilometry od miejsca zamieszkania", "Africa Dreamer","C:\\Users\\Programista\\Desktop\\kaktus1.png"}
        };
    }


    @Test(dataProvider = "getData")
    public void testAddAd(String name, String pass, String tytul, String cena, String description, String user, String photolink) throws AWTException {

        WebDriver driver = getDriver();

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        AddAdPage addAdPage = PageFactory.initElements(driver, AddAdPage.class);
        loginPage.loginToApp(name, pass);
        addAdPage.addAd(tytul, cena, description, user, photolink);

        Assert.assertTrue(driver.findElement(By.id("save-from-preview")).isDisplayed());

        helper.getScreenShot("WEB: Add Ad Test.png");
    }

    @AfterMethod
    public void tearDown() {

        WebDriver driver = getDriver();

        System.out.println("Test ended");
        driver.close();
    }
}
