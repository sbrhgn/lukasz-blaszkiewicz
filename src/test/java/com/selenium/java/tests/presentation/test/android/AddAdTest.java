package com.selenium.java.tests.presentation.test.android;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import com.selenium.java.base.pages.android.AddAdScreen;
import com.selenium.java.base.pages.android.LoginScreen;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;


public class AddAdTest extends BaseTest {

    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceID, String deviceName, ITestContext context) {

        instalApp = false;

        launchAndroid(platform, deviceID, deviceName, context);

    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
                {"celestiallucifer@gmail.com", "Xxxxxx11pkdw", "Nasturcja", "50", "Roślina ogrodowa, szybko rosnąca tudzież w ogóle", "Nasturcja Lover"},
                {"celestiallucifer@gmail.com", "Xxxxxx11pkdw", "Mak", "150", "Opium - idealny starter pack dla początkujących", "Marek Makowski"},
                {"celestiallucifer@gmail.com", "Xxxxxx11pkdw", "Paprotka", "5", "Rosnaca nieprzerwanie od czasu PRL", "UB General"},
                {"celestiallucifer@gmail.com", "Xxxxxx11pkdw", "Róża", "8", "Likwidacja kwiaciarni - tylko priv", "Mariola Kwiatkowska"},
                {"celestiallucifer@gmail.com", "Xxxxxx11pkdw", "Kaktus", "20", "Latwy w utrzymaniu kwiat; polecany gdy studnia znajduje się 3 kilometry od miejsca zamieszkania", "Africa Dreamer"}
        };
    }

    MethodHelper helper = new MethodHelper();

    @Test(dataProvider = "getData")
    public void testLoginToApp(String name, String password, String tytul, String cena, String trescOgloszenia, String uzytkownik) {

        WebDriver driver = getDriver();

        AddAdScreen addAdScreen = PageFactory.initElements(driver, AddAdScreen.class);
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginToApp(name, password);
        loginScreen.loginToAppTwo();
        addAdScreen.addAdScreen(tytul, cena, trescOgloszenia, uzytkownik);

        helper.waitTime(5);

        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/submit")).isDisplayed());
        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/back")).isDisplayed());

        helper.getScreenShot("Dodanie ogłoszenia.png");

    }

    @AfterMethod
    public void tearDown() {

        System.out.println((char) 27 + "[35m" + "Koniec testu dodania ogloszenia");
    }
}



