package com.selenium.java.tests.presentation.test.web;


import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import com.selenium.java.base.pages.android.CreateAccountScreen;
import com.selenium.java.base.pages.android.LoginScreen;
import com.selenium.java.base.pages.web.LoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.lang.reflect.Method;


public class LoginWebTest extends BaseTest {

    @BeforeMethod
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false; //uncomment if u want a headless mode
        url = "https://www.olx.pl";

        launchWeb(platform, browser, context);
        System.out.println("Test started");
    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
                {"lukasz_tester_1@wp.pl", "Tester123##"},
                {"lukasz_tester_2@wp.pl", "Tester123##"},
//                {"lukasz_tester_333@wp.pl", "Tester123##"},
//                {"lukasz_tester_444@wp.pl", "Tester123##"},
//                {"lukasz_tester_555@wp.pl", "Tester123##"},
        };
    }

    @DataProvider
    public Object[][] getLoginOnly() {
        return new Object[][]{
                {"lukasz_tester_1x1@wp.pl", ""},
                {"lukasz_tester_2x2wp.pl", ""},
                {"lukasz_tester_3x3@wp.pl", ""},
                {"lukasz_tester_4x4@wp.pl", ""},
                {"lukasz_tester_5x5@wp.pl", ""},
        };
    }

    @DataProvider
    public Object[][] getPasswordOnly() {
        return new Object[][]{
                {"", "Tester123##"},
                {"", "Tester123##"},
                {"", "Tester123##"},
                {"", "Tester123##"},
                {"", "Tester123##"},
        };
    }

    @DataProvider
    public Object[][] getWrongMail() {
        return new Object[][]{
                {"lukasz_tester_1@w@p.pl", "Tester123##"},
                {"lukasz_tester_1@wpp", "Tester123##"},
                {"lukasz_tester_1@", "Tester123##"},
                {"lukasz_tester_1", "Tester123##"},
                {"@wp.pl", "Tester123##"},
        };
    }

    @DataProvider
    public Object[][] getWrongPassword() {
        return new Object[][]{
                {"lukasz_tester_1q1x@wp.pl", "tester123##"},
                {"lukasz_tester_2q2x@wp.pl", "tester"},
                {"lukasz_tester_3q3x@wp.pl", "tester123"},
                {"lukasz_tester_4q4x@wp.pl", "Tester"},
                {"lukasz_tester_5q5x@wp.pl", "lukasz_tester_5@wp.pl"},
        };
    }

    MethodHelper helper = new MethodHelper();

    @Test(dataProvider = "getData")
    public void testLoginAccount(String name, String pass) {

        WebDriver driver = getDriver();
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.loginToApp(name, pass);
        helper.waitTime(2);

        try {
            driver.findElement(By.className("paybalance-box__balance"));
        } catch (NoSuchElementException e) {
            Assert.fail("Error on Page: element not found");
        }
        Assert.assertTrue(driver.findElement(By.className("paybalance-box__balance")).isDisplayed());


        helper.getScreenShot("WEB: Login web test.png");
        driver.close();
    }

    @Test(dataProvider = "getLoginOnly")
    public void testLoginWithoutPassword(String name, String pass) {
        WebDriver driver = getDriver();
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.loginToApp(name, pass);
        Assert.assertTrue(driver.findElement(By.id("se_userLogin")).isDisplayed());

        helper.getScreenShot("WEB: Login without password.png");
        driver.close();
    }

    @Test(dataProvider = "getPasswordOnly")
    public void testLoginWithoutLogin(String name, String pass) {
        WebDriver driver = getDriver();
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.loginToApp(name, pass);
        Assert.assertTrue(driver.findElement(By.id("se_userLogin")).isDisplayed());

        helper.getScreenShot("WEB: Login without password.png");
        driver.close();
    }

    @Test(dataProvider = "getWrongMail")
    public void testLoginWithWrongMail(String name, String pass) {

        WebDriver driver = getDriver();
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.loginToApp(name, pass);
        Assert.assertTrue(driver.findElement(By.id("se_userLogin")).isDisplayed());

        helper.getScreenShot("WEB: Login with wrong mail.png");
        driver.close();
    }

    @Test(dataProvider = "getWrongPassword")
    public void testLoginWithWrongPassword(String name, String pass) {

        WebDriver driver = getDriver();
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.loginToApp(name, pass);
        Assert.assertTrue(driver.findElement(By.id("se_userLogin")).isDisplayed());

        helper.getScreenShot("WEB: Login with wrong password.png");
        driver.close();
    }

    @AfterMethod
    public void tearDown() {

        System.out.println((char) 27 + "[35m" + "Koniec testów zakładania konta");
    }
}



